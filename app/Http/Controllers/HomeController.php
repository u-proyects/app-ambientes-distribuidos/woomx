<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the Posts.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')->get();

        for ($i = 0; $i < count($posts); $i++) {
            $iduser = $posts[$i]->userid;
            $userinfo = User::findOrFail($iduser);
            $posts[$i]["userinfo"] =  $userinfo;

            $postDate = Carbon::parse( $posts[$i]->created_at );

            $relativeTime = $postDate->diffForHumans();

            $posts[$i]['relativetime'] = $relativeTime;
        }

        return view('home', ['posts' => $posts]);
    }

    /**
     * Create a new Post instance.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'text' => 'required|max:255',
        ]);
    
        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }

        $post = new Post;
        $post->userid = $request->userid;
        $post->text = $request->text;
        $post->save();

        return redirect('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::findOrFail($id)->delete();
        return redirect()->route('home')->with('success','Post deleted successfully!');
    }

}
