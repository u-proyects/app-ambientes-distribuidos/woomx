
@extends('layouts.app')

@section('content')
<div class="main-wrapper">

    <div class="top">
        <img src="/img/woomx.png" alt="logo" width="150">
        <h1>We welcome you back!</h1>
    </div>

    <div class="fullform">
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <b-form-group label="E-Mail Address">
                <b-form-input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email"></b-form-input>
            </b-form-group>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

            <b-form-group label="Password">
                <b-form-input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password"></b-form-input>
            </b-form-group>
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                <label class="form-check-label" for="remember">
                    {{ __('Remember Me') }}
                </label>
            </div>

            <br>
            <b-button type="submit" class="mbtn2" block>Log in</b-button>
            <br>
            <div class="text-center">
                @if (Route::has('password.request'))
                    <a href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                @endif
                <br>
                <br>
                <p>
                    New to WooMx? <a href="{{ route('register') }}"> Join now!</a>
                    
                </p>
            </div>

            

        </form>

    </div>

</div>
@endsection
