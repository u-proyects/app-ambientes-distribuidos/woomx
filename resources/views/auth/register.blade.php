@extends('layouts.app')

@section('content')
<div class="main-wrapper">

    <div class="top">
        <img src="/img/woomxwhite.png" alt="logo" width="150">
        <h1>Join us now!</h1>
    </div>

    <div class="fullform shadow">
        <form method="POST" action="{{ route('register') }}">
            @csrf

            <b-form-group label="Name">
                <b-form-input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus></b-form-input>
            </b-form-group>
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

            <b-form-group label="E-Mail Address">
                <b-form-input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email"></b-form-input>
            </b-form-group>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

            <b-form-group label="Password">
                <b-form-input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password"></b-form-input>
            </b-form-group>
            @error('password')
                <h2 class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </h2>
            @enderror

            <b-form-group label="Confirm Password">
                <b-form-input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password"></b-form-input>
            </b-form-group>

            <br>
            <b-button class="mbtn1" type="submit"  block>Join now!</b-button>

        </form>

    </div>

</div>

@endsection
