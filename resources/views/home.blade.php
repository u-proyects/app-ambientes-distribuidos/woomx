@extends('layouts.app')

@section('content')
<div class="main-wrapper">

  <div class="profile">
    <user-info user-name="{{ Auth::user()->name }}"></user-info>
  </div>

  <div class="feed">

    <form action="{{ route('home') }}" method="POST">
      @csrf
      <post-maker 
      user-id="{{ Auth::user()->id }}" 
      user-name="{{ Auth::user()->name }}"
      ></post-maker>
    </form>

    <hr>

    @if ($message = Session::get('success'))
      <div class="ml-auto mr-auto alert alert-success">
        <p>{{ $message }}</p>
      </div>
    @endif

    @if ( count($posts) > 0 )
      @foreach ($posts as $post)

      <form action="{{ route('home/{id}', $post->id)}}" method="POST">
        @csrf
        @method('DELETE')

        <post 
          date="{{$post->relativetime}}"
          comment-user-id="{{$post->userid}}"
          comment-user-name="{{$post->userinfo->name }}"
          text="{{$post->text}}"
          user-id="{{ Auth::user()->id }}"
          user-role="{{ Auth::user()->role }}"
          ></post>

      </form>
        
      @endforeach
    @endif

  </div>

  <div class="news">

  </div>
</div>
@endsection
