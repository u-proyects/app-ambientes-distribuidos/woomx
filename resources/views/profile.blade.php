@extends('layouts.app')

@section('content')
<div class="main-wrapper">

  <div class="main-info ml-auto mr-auto">

    <div class="text-center">
      <avatar class="img ml-auto mr-auto" background-color="#f4508e" color="#fff" username="{{ Auth::user()->name }}" :size="150"></avatar> 

      <h1 class="">{{ Auth::user()->name }}</h1>

      <b-button class="ml-auto mr-auto" variant="outline-primary">
        Follow
      </b-button>
    </div>

    <hr>

    <div>
      <h5>About me</h5>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta nobis, aperiam sequi dolore voluptate ea eligendi debitis doloremque culpa, aut assumenda, obcaecati quis perspiciatis inventore quaerat esse itaque similique soluta!
      </p>
    </div>

  
  </div>

  <hr>

  <div class="feed">

    <form action="{{ route('profile') }}" method="POST">
      @csrf
      <post-maker 
      user-id="{{ Auth::user()->id }}" 
      user-name="{{ Auth::user()->name }}"
      ></post-maker>
    </form>

    <hr>

    @if ($message = Session::get('success'))
      <div class="ml-auto mr-auto alert alert-success">
        <p>{{ $message }}</p>
      </div>
    @endif

    @if ( count($posts) > 0 )
      @foreach ($posts as $post)

      <form action="{{ route('profile/{id}', $post->id)}}" method="POST">
        @csrf
        @method('DELETE')
        <post 
          date="{{$post->relativetime}}"
          comment-user-id="{{$post->userid}}"
          comment-user-name="{{$post->userinfo->name }}"
          text="{{$post->text}}"
          user-id="{{ Auth::user()->id }}"
          user-role="{{ Auth::user()->role }}"
          ></post>

      </form>

        
      @endforeach
    @endif

  </div>

</div>
@endsection