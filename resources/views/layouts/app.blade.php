<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'WooMx') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/'. Route::currentRouteName() .'.css') }}" rel="stylesheet">


</head>
<body>
    <div id="app">

            @if (Route::currentRouteName() == "" || Route::currentRouteName() == "login")
            <nav class="navbar navbar-expand-md navbar-light bg-white">

            @else
            <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">

            @endif
                <div class="container">

                    @guest
                    <a class="navbar-brand" href="{{ url('/') }}">
                    {{-- config('app.name', 'WooMx') --}}
                    <img src="/img/woomx.png" alt="logo" width="150">
                    </a>
                    @else
                    <a class="navbar-brand" href="/home">
                    {{-- config('app.name', 'WooMx') --}}
                    <img src="/img/woomx.png" alt="logo" width="150">
                    </a>
                    
                    @endguest


                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">

                            <!-- Authentication Links -->
                            @guest                          
                                @if (Route::has('register'))
                                    <b-button class="mbtn1" href="{{ route('register') }}">Join Now</b-button>
                                @endif

                                @if (Route::has('login'))
                                    <li class="nav-item">
                                        <b-button class="mbtn2" href="{{ route('login') }}" >Log In</b-button>
                                    </li>
                                @endif

                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }}
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest

                        </ul>
                    </div>
                </div>
            </nav>
        

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
