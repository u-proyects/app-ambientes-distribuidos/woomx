
@extends('layouts.app')

@section('content')
<div class="main-wrapper">
    <b-row class="section">
        <b-col>

            

            @guest 
            <h1>Welcome to your community!</h1>
            <form class="text-center" method="POST" action="{{ route('login') }}">
            @csrf

                <b-form-input id="email" type="email" class="ml-auto mr-auto mb-4 form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="E-Mail"></b-form-input>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <b-form-input id="password" type="password" class="ml-auto mr-auto mb-4 form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password"></b-form-input>
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>

                <br>
                <b-button type="submit" class="mbtn2 ml-auto mr-auto" block>Log in</b-button>
                <br>
                <div class="text-center">
                    @if (Route::has('password.request'))
                        <a href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    @endif
                    <br>
                    <br>
                    <p>
                        New to WooMx? <a href="{{ route('register') }}"> Join now!</a>
                        
                    </p>
                </div>
            </form>
            @else
                <h1>Hi {{ Auth::user()->name }}!</h1>
                <b-button class="mbtn2 ml-auto mr-auto" href="/home" block>Home</b-button>
            @endguest

        </b-col>

        <b-col> 
            <img class="ml-auto mr-auto" src="https://static-exp3.licdn.com/sc/h/3b678qey22i0i8cxykw5gjupc" alt="first-img" width="700"> 
        </b-col>

    </b-row>

<!--
    <b-row class="section">
        <b-col> <h3>Find relevant and verified information</h3> </b-col>

        <b-col> 
            <h6>Suggested Searches</h6>

            <div>Option 1</div>
            <div>Option 2</div>
            <div>Option 3</div>
        </b-col>

    </b-row>
-->

</div>
@endsection