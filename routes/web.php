<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('welcome');
})->name('welcome');



Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::post('/home', [HomeController::class, 'store'])->name('home');

Route::delete('/home/{id}', [HomeController::class, 'destroy'])->name('home/{id}');


/* ------------------------------------------------------------------------*/

Route::get('/profile', function () {
    return view('profile');
});


Route::get('/profile', [ProfileController::class, 'index'])->name('profile');

Route::post('/profile', [ProfileController::class, 'store'])->name('profile');

Route::delete('/profile/{id}', [ProfileController::class, 'destroy'])->name('profile/{id}');
